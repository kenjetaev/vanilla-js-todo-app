const reminderStorage = [];
const listStorage = document.querySelector('input');
const outputList = document.querySelector('.output-list');


listStorage.addEventListener('keypress', (add) => {
    if (add.keyCode === 13) {
        if (listStorage.value !== '') {
            reminderStorage.push(listStorage.value);
            outputList.innerHTML += `<div>${listStorage.value}<span class="btn"><i class="fas fa-trash"></i></span></div>`;
            listStorage.value = '';
            const buttons = document.querySelectorAll('span');
            buttons.forEach((btn) => {
                btn.addEventListener('click', () => {
                    let idx = reminderStorage.indexOf(btn.parentElement.innerText)
                    reminderStorage.splice(idx, 1);
                    btn.parentElement.remove()
                    console.log(reminderStorage)
                })
            })
            console.log(reminderStorage)
        }
    }
})